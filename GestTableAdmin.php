<?php
/* ---------------------- // 
 * gestion de mise a jours 
 * pour l'objet GestTable
 * ---------------------- */
class GestTableAdmin extends GestTable{

    // - --------------------------- - //
    // = traitement des mise a jours = //
    // - --------------------------- - //

    // - appelle les fonctions de mise  a jours (GET/POST) - //
    // function modele déstiné a etre surchargé
    public function checkUpdate(){
        $tag=$this->tag;

        // - traitement de la suppression - //
        if (isset($_GET[$tag.'Delete'])){
            if ($id=$this->delete((int)$_GET[$tag.'Delete'])){
                $this->setPagination(); // -- repagination -- //
                $this->notifs->set(NOTIFS::DELETE,'Le no '.$id.' &agrave; bien &eacute;t&eacute; supprim&eacute;!');
            }
            else{ // pb lors de la suppression
            }
        }

        // - traitement d'un ajout/modification - //
        if ($this->add() === 1){
            $this->setPagination(); // -- repagination -- //
            $this->notifs->set(NOTIFS::ADD,'Un nouvel &eacute;l&eacute;ment &agrave; &eacute;t&eacute; cr&eacute;&eacute;!');
        }

        $id=$this->update();
        if ($id !== NULL){
            //$this->setPagination(); // -- pas de repagination car pas de changement de nb d'elt-- //
            $this->notifs->set(NOTIFS::UPDATE,'Le num&eacute;ro '.$id.' &agrave; bien &eacute;t&eacute; modifi&eacute;!');
        }
    }//function checkUpdate()


    // - Insert une ligne - //
    // fontion exemple/test destinée à etre surchargé
    // @return 
    //  - 1 :si ajouter
    //  - NULL si fonction non demander
    public function add(){
        $tag=$this->tag().'Add';

        if (isset($_POST[$tag])){
            if (ISDEV === 1) echo gestLib_inspect('$tag',$tag,__FUNCTION__.':'.__LINE__,__FILE__);
            $jour=(int)$_POST['jour'];
            $mois=(int)$_POST['mois'];
            $fete=$_POST['fete'];
            $type=$_POST['type'];
            if (ISDEV === 1) echo gestLib_inspect('$type',$type,__FUNCTION__.':'.__LINE__,__FILE__);
            $values='VALUES ( :jour , :mois , :fete , :_type )';
            $this->setSqlLast('INSERT INTO '.$this->tableNom.'( `jour`, `mois`, `fete`, `type`) '.$values);

            try{            
                $requete=$this->db->prepare($this->sqlLast());
                $requete->bindValue(':jour',$jour, PDO::PARAM_INT);
                $requete->bindValue(':mois',$mois, PDO::PARAM_INT);
                $requete->bindValue(':fete',$fete, PDO::PARAM_STR);
                $requete->bindValue(':_type',$type, PDO::PARAM_STR);
                $requete->execute();
            }
            catch(PDOException $e){
                $errTxt=$e->getMessage();
                echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.nl2br($this->sqlLast()).'</div>';
                echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.$errTxt.'</div>';
                return NULL;

            }
            return 1;
        }
        return NULL;
    } // function add()


    // - update une ligne - //
    // declencher via $_POST[$tag.'Set']
    // fonction exemple/test destinée à etre surchargé
    // @return :
    //  - l'indexId du champ modifié
    //  - NULL: si la fonction n'est pas demander
    public function update(){
        $tag=$this->tag();
        if (isset($_POST[$tag.'Set'])){

            $id=(int)$_POST['jours_id'];
            $jour=(int)$_POST['jour'];
            $mois=(int)$_POST['mois'];
            $fete=$_POST['fete'];
            $type=$_POST['type'];

            $set=' `jour` := :jour, `mois` := :mois, `fete` := :fete, `type` := :type';
            $this->setSqlLast('UPDATE '.$this->tableNom.' SET '.$set.' WHERE `jours_id` = '.$id);
            
            try{ 
                $requete=$this->db->prepare($this->sqlLast());
                $requete->bindValue(':jour',$jour, PDO::PARAM_INT);
                $requete->bindValue(':mois',$mois, PDO::PARAM_INT);
                $requete->bindValue(':fete',$fete, PDO::PARAM_STR);
                $requete->bindValue(':type',$type, PDO::PARAM_STR);
                $requete->execute();
                return  $id;
            }
            catch(PDOException $e){
                $errTxt=$e->getMessage();
                echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.nl2br($this->sqlLast()).'</div>';
                echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.$errTxt.'</div>';
                return NULL;
            }
        } // if (isset($_POST[$tag.'Set']))
        return NULL;
    } // function update()

    
    // - Reception d'une demande de suppression d'un record - //
    // @id : (int) no du champ a effacer
    // @return :
    //   1 si une demande a été faite avec ce numéro
    //   0 sinon
    // - //
    public function isDelAsk($id){
        $tag=$this->tag.'DelAsk';
        if (isset($_GET[$tag]))return ($id === (int)$_GET[$tag])?1:0;
        return 0;
    }


    // -   / suppression de ligne - //
    // @id : (int) no du champ a effacer
    public function delete($id){
        $id=(int)$id;
        if ($id===0) return 0;

        $this->sqls[]=$this->sqlLast='DELETE FROM `'.$this->tableNom.'` WHERE '.$this->indexNom.'='.$id;
        $this->db->exec($this->sqlLast);
        return $id;
    }

    // - --------- - //
    // - affichage - //
    // - --------- - //

    // - retourne les champs d'une ligne sous forme de champs de formulaire - //
    // fonction exemple/test destinée à etre surchargé
    // @return html
    public function showFormEditContentAll(){
        $o='';
        foreach ($this->table() as $id => $ligne){
            $o.=$this->showFormEditContent($id,$showId=1);
        }
        return $o;
    }//function showFormEditContentAll()


    // - affiche les champs d'une ligne sous forme de champs de formulaire - //
    // fonction exemple/test destinée à etre surchargé
    // @id : 
    //  - non NULL: remplir les champs avec les valeurs du champs de l'index
    //  - NULL: pas de valeur par defaut
    //  @showId : !==0 : ajoute l'id a chaque nom de champ du formulaire
    // @return html
    public function showFormEditContent($id=NULL,$showId=0){
        $tag=$this->tag;
        
        $o='';
        if ($id === NULL){
            $o.='<a name="'.$tag.'Add"></a>';
            foreach ($this->table($this->indexMax()) as $fielName => $val){
                $o.='<span class="labelChamp">';
                $o.='<label for="'.$fielName.'">'.$fielName.'</label>: ';
                $o.='<input name="'.$fielName.'" id="'.$fielName.'" placeholder="" value="" />';
                $o.='</span>'."\n";
            } // foreach
        }
        else{
            $o.='<a name="'.$tag.'Set'.$id.'"></a>';
            if ($this->isDelAsk($id) === 0){
                $o.='<a href="?'.$tag.'DelAsk='.$id.'&amp;GestTable=GestTableAdmin#'.$tag.'Set'.$id.'">Demande de suppression</a>';
            }
            else{
                $o.=' - <a href="?'.$tag.'Delete='.$id.'&amp;GestTable=GestTableAdmin">Supprimer</a>';
            }
            $o.='<br />';

            if ($showId === 0){   
                foreach ($this->table($id) as $fielName => $val){
                    $o.='<span class="labelChamp">';
                    $o.='<label for="'.$fielName.'">'.$fielName.'</label>: ';
                    $o.='<input name="'.$fielName.'" id="'.$fielName.'" placeholder="" value="'.$val.'" />';
                    $o.='</span>'."\n";
                } // foreach
            }
            else{
                foreach ($this->table($id) as $fielName => $val){
                    $o.='<span class="labelChamp">';
                    $o.='<labelfor="'.$fielName.$id.'">'.$fielName.$id.'</label>: ';
                    $o.='<input name="'.$fielName.$id.'" id="'.$fielName.$id.'" placeholder="" value="'.$val.'" />';
                    $o.='</span>'."\n";
                } // foreach
            }
        }    
        return $o.'<br />';
    }//function showFormEditContent


    // - affiche les champs d'une ligne sous forme de champs de formulaire - //
    // fonction exemple/test destinée à etre surchargé
    // @return html



    public function showNotifs(){
        $o='';
        // - Afficher la notif d'édition - //
        if($this->notifs->is(Notifs::ADD)){
            $o.= '<div class="notifclassic">'.$this->notifs->get(Notifs::ADD).'</div>';
        }

        // - Afficher la notif d'édition - //
        if($this->notifs->is(Notifs::UPDATE)){
            $o.= '<div class="notifclassic">'.$this->notifs->get(Notifs::UPDATE).'</div>';
        }

        // - Afficher la notif de destruction - //
        if($this->notifs->is(Notifs::DELETE)){
            $o.= '<div class="notifclassic">'.$this->notifs->get(Notifs::DELETE).'</div>';
        }

        return $o;
    }// function showNotifs()
    
} // lass GestTableAdmin extends GestTable
