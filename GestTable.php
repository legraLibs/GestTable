<?php
/*
 * class GestTable()
 * gestion simple d'une table sqp via PDO
 * Ne contient aucune fonction de mise a jours (voir GestTaleAdmin()
 */ 
define ('GESTTABLE_VERSION','0.0.1-dev');
$gestLib->loadLib('GestTable',__FILE__,GESTTABLE_VERSION,'gestionnaire de table sql');
class GestTable{
    const GESTTABLE_VERSION = GESTTABLE_VERSION;
    protected $db;
    protected $tableNom;
    protected $indexNom;
    protected $table;    // contient les enregistrements
    protected $tag;      // prefixe des variables lors des appelles systèmes (GET/POST/etc)

    // - WGO - //
    protected $where;
    protected $groupby;
    protected $orderby;
    protected $orderDir;    // '' ou ' DESC'

    // - pagination - //
    protected $pageNo;
    protected $pageNb;
    protected $eltInDb;     // nombre TOTAL d'element dans la db et non le nbr d'elt selectionnée
    protected $eltByPage;
    protected $eltFirst;
    protected $eltLast;


    // - notifications - //
    public $notifs;

    // - debug - //
    private $sqlLast;    // dernier code sqlLast executer
    protected $sqls;    // tableau des sql successifs

    const NOCACHE = 0;
    const CACHE = 1;
    
    public function __construct(PDO $db,$tableNom,$indexNom,$tag=NULL){
        
        if (DEBUG_LVL === 1)echo gestLib_inspect('',NULL,__FUNCTION__.':'.__LINE__,__FILE__);
        $this->db = $db;
        $this->tableNom = $tableNom;
        $this->indexNom = $indexNom;
        $this->table=[];
        $this->tag = $tag===NULL?$tableNom:$tag;

        // - WGO - //
        $this->where='';
        $this->groupby='';
        $this->orderby=''; // ex: 'id nom ASC, prenom DESC'

        // - pagination - //
        $this->eltInDb(GestTable::NOCACHE); // mise a jours du nombre de ligne TOTAL dans la table (et mise en cache)
        $this->pageNo=1;
        $this->pageNb=1;    // caculer lors de setPagination
        $this->eltByPage=0; // par defaut toutes les pages
        $this->eltFirst=1;  // indexNo du 1er element
        $this->eltLast=1;   // offset

        // - notifications - //
        //$this->notifs= new Notifs();

        // - debug - //
        $this->sqlLast='';
        $this->sqls=[];
    }


    // - ----------- - //
    // - hydratation - //
    // - ----------- - //

    public function indexNom(){
        return $this->indexNom;
    }

    public function tag(){
        return $this->tag;
    }

    // - renvoie le cache en entier ou l'id precisé - //
    // @id : id de l'ind (si NULL renvoie toute la table)
    // @fieldNom : (facultatif) nom du champ a renvoyer
    // @return NULL si indice inexistant
    public function table($id=NULL,$fieldNom=NULL){
        if ($id === NULL) return $this->table;
        if ($fieldNom === NULL){
            return isset($this->table[$id])?$this->table[$id]:NULL;
        }
        else{
            return isset($this->table[$id][$fieldNom])?$this->table[$id][$fieldNom]:NULL;
        }
    }


    // -- WGO -- //
    public function where(){
        return $this->where;
    }

    public function setWhere($val){
        $this->where=$val;
    }

    public function groupby(){
        return $this->groupby;
    }

    public function setGroupby($val){
        $this->groupby=$val;
    }

    public function orderby(){
        return $this->orderby;
    }

    public function setOrderby($val){
        $this->orderby=$val;
    }


    // - pagination - //
    public function pageNb(){
        return $this->pageNb;
    }

    public function setPageNb($val){
        $this->pageNb=(int)$val;
    }

    // = gesstion du numero de page = //

    // -- pageNo -- //
    public function pageNo(){
        return $this->pageNo;
    }

    // definit le numero de la page a afficher
    // @eltNb : (int) nombre d'element
    // si NULL definit automatiquement
    // @auto : (bool)
    //  true: calcul la valeur via GPS (GetPostSession)
    //  false: c'est une valeur direct
    // @depend : $this->pageNb
    public function setPageNo($pageNo=NULL,$auto=true){
        if (DEBUG_LVL === 1)echo gestLib_inspect('$this->pageNo',$this->pageNo,__FUNCTION__.':'.__LINE__,__FILE__);
        $tag=$this->tag.'_pageNo';
        if (ISDEV >= 1)echo gestLib_inspect('$tag',$tag,__FUNCTION__.':'.__LINE__,__FILE__);

        if ($auto === true){
            if (isset($_GET[$tag])){$this->pageNo=(int)$_GET[$tag];}
            elseif (isset($_POST[$tag])){$this->pageNo=(int)$_POST[$tag];}
            elseif (isset($_SESSION[$tag])){$this->pageNo=(int)$_SESSION[$tag];}
            elseif ($pageNo !== NULL){$this->pageNo=(int)$pageNo;}
            else $this->pageNo=1;
        }
        else{
            $this->pageNo=(int)$pageNo;
        }


        // limiter a la page entre 1 et maxi
        if ($this->pageNo < 1)$this->pageNo=1;
        elseif ($this->pageNo > $this->pageNb)$this->pageNo=$this->pageNb;

        $_SESSION[$tag]=$this->pageNo;
        if (ISDEV >= 1)echo gestLib_inspect('$this->pageNo',$this->pageNo,__FUNCTION__.':'.__LINE__,__FILE__);
    }


    //  modifie pageNo en selon $offset (-/+)
    //  @offset : nombre de page positive ou negative a ajouter a pageNo
    //   - si non defini: aucune action
    public function relPageNo($offset=0){
        if (ISDEV >= 1)echo '<br>'.gestLib_inspect('$this->pageNo',$this->pageNo,__FUNCTION__.':'.__LINE__,__FILE__);
        $tag=$this->tag.'_relPage';

        if (isset($_GET[$tag])){$offset=(int)$_GET[$tag];}
        elseif (isset($_POST[$tag])){$offset=(int)$_POST[$tag];}
        elseif ($offset !== NULL)$offset=(int)$offset;
        else $offset=0;

        if (ISDEV >= 1)echo gestLib_inspect('$offset',$offset,__FUNCTION__.':'.__LINE__,__FILE__);
        $newPage=$this->pageNo+$offset;
        if (ISDEV >= 1)echo gestLib_inspect('$newPage',$newPage,__FUNCTION__.':'.__LINE__,__FILE__);
        $this->setPageNo($this->pageNo+$offset,$auto=false);
        if (ISDEV >= 1)echo gestLib_inspect('$this->pageNo',$this->pageNo,__FUNCTION__.':'.__LINE__,__FILE__);
    }


    // -- eltByPage -- //
    public function eltByPage(){
        return $this->eltByPage;
    }

    // definit le nombre d'element a afficher par page
    // @eltNb : (int) nombre d'element
    //  - NULL definit automatiquement
    //  - -1 : recharge le nombre via la db
    // @depend : 
    public function setEltByPage($eltNb=NULL){
        $tag=$this->tag.'_eltByPage';
    
        if ($eltNb === -1){$this->eltByPage=$this->eltInDb;} // prioritaire

        elseif (isset($_GET[$tag])){$this->eltByPage=(int)$_GET[$tag];}
        elseif (isset($_POST[$tag])){$this->eltByPage=(int)$_POST[$tag];}
        elseif (isset($_SESSION[$tag])){$this->eltByPage=(int)$_SESSION[$tag];}
        elseif ($eltNb !== NULL){$this->eltByPage=(int)$eltNb;}
        else $this->eltByPage=$this->eltInDb;// 

        // limiter a 0 entre 1 et eltInDB
        if ($this->eltByPage === 0)
            $this->eltByPage=$this->eltInDb;
        elseif ($this->eltByPage < 1)$this->eltByPage=1;
        elseif ($this->eltByPage > $this->eltInDb)$this->eltByPage=$this->eltInDb;
        

        $_SESSION[$tag]=$this->eltByPage;
        if (DEBUG_LVL === 1)echo gestLib_inspect('$this->eltByPage',$this->eltByPage,__FUNCTION__.':'.__LINE__,__FILE__);
    }


    // -- renvoie le nombre d'element de la table (a partir du cache ou de la db) -- //
    // @cache :
    //  GestTable::NOCACHE : la valeur est lu via sql
    //  GestTable::CACHE : la valeur est lu via le cache
    public function eltInDb($cache=GestTable::CACHE){
        if (DEBUG_LVL === 1)echo gestLib_inspect('',NULL,__FUNCTION__.':'.__LINE__,__FILE__);
        if ($cache === GestTable::CACHE)
            return $this->eltInDb;
        $this->setSqlLast('SELECT count(*) AS eltInDb FROM '.$this->tableNom);
        $rep=$this->db->query($this->sqlLast);
        $ligne=$rep->fetch(PDO::FETCH_ASSOC);
        $this->eltInDb=(int)$ligne['eltInDb'];
        $rep->closeCursor();
        return $this->eltInDb;
    }

    public function eltFirst(){
        return $this->eltFirst;
    }


    // -- debug:sql -- //
    public function sqlLast(){
        return $this->sqlLast;
    }
    
    public function setSqlLast($sql){
        $this->sqls[]=$this->sqlLast=$sql;
    }

    public function sqls(){
        return $this->sqls;
    }


    // - ---------- - //
    // - chargement - //
    // - ---------- - //
    // - charge les donnees depuis la db - //
    // pagination:
    // @pageNu : (int) numero de la page a afficher
    // @eltByPage : (int) nombre d'element a afficher par page (cf setPagination() pour les vleurs)
    public function load($pageNo=1,$eltByPage=-1){

        $this->setPagination($pageNo,$eltByPage);
        if(ISDEV > 1)echo gestLib_inspect('$this->eltByPage',$this->eltByPage,__FUNCTION__.':'.__LINE__,__FILE__);

        // -- construit la requete sqlLast -- //
        $where=$this->where!==''?' WHERE '.$this->where.'':'';
        $groupby=$this->groupby!==''?' GROUP BY '.$this->groupby.'':'';
        $orderby=$this->orderby!==''?' ORDER BY '.$this->orderby:'';
        $this->setSqlLast('SELECT * FROM `'.$this->tableNom.'`'.$where.$groupby.$orderby.' LIMIT '.$this->eltFirst.','.$this->eltByPage);
        $this->table=[];

        // -- query:chargement des lignes selectionées -- //
        $rep=$this->db->query($this->sqlLast);
        while ($ligne=$rep->fetch(PDO::FETCH_ASSOC)){
            $id=$ligne[$this->indexNom];
            $this->table[$id]=array();
            foreach($ligne as $key => $val)$this->table[$id][$key]=is_numeric($val)?(int)$val:$val;
        }
        $rep->closeCursor();

        return $this->sqlLast;
    } //function load()


    // - ---------- - //
    // - pagination - //
    // - ---------- - //

    // - calcul de la pagination - //
    // @pageNo : numero de la page a atteindre
    // @eltByPage : nombre d'elt a afficher par page (defaut: tous)
    public function setPagination($pageNo=1,$eltByPage=0){
        
        $this->setEltByPage($eltByPage);
        if (ISDEV > 0)echo gestLib_inspect('$eltByPage',$eltByPage,__FUNCTION__.':'.__LINE__,__FILE__);
        if(ISDEV > 1)echo gestLib_inspect('$this->eltByPage',$this->eltByPage,__FUNCTION__.':'.__LINE__,__FILE__);

        if ($this->eltByPage === 0){// toutes les pages
            $this->setPageNb(1);
            $this->setPageNo($pageNo); // APRES le calcul de pageNb  AVANT setEltByPage
            $this->relPageNo();
            $this->setEltByPage($this->eltInDb);
            $this->eltFirst=0;
            $this->eltLast=$this->eltInDb;
            if(ISDEV > 1)echo gestLib_inspect('$this->eltByPage(TOUTES LES PAGES)',$this->eltByPage,__FUNCTION__.':'.__LINE__,__FILE__);
        }
        else{
            $this->setPageNb((int)($this->eltInDb() / $this->eltByPage));

            $restant=$this->pageNb % $this->eltByPage;
            if (ISDEV >= 1)echo gestLib_inspect('$restant',$restant,__FUNCTION__.':'.__LINE__,__FILE__);
            if ($restant !== 0) $this->pageNb+=1;

            $this->setPageNo($pageNo); // APRES le calcul de pageNb  AVANT eltFirst
            $this->relPageNo();
            $this->eltFirst= $this->eltByPage * ($this->pageNo-1);
            $this->eltLast= $this->eltFirst + $this->eltByPage;
            if(ISDEV > 1)echo gestLib_inspect('$this->eltByPage',$this->eltByPage,__FUNCTION__.':'.__LINE__,__FILE__);
        }
    } //function setPagination()



    // - --- - //
    // - sql - //
    // - --- - //

    // retourne le dernier index de la table
    public function lastInsertId(){
        return $this->db->lastInsertId();
    }

    // - Renvoie la valeur max de l'index dans $table - //
    public function indexMax(){
        return max(array_keys($this->table));
    }


    // - Renvoie la valeur max de l'index dans la db - //
    public function indexMaxInDb(){
        $this->setSqlLast('SELECT MAX('.$this->indexNom.') AS val FROM '.$this->tableNom);
        $rep=$this->db->query($this->sqlLast);
        $ligne=$rep->fetch(PDO::FETCH_ASSOC);
        $this->val=(int)$ligne['val'];
        $rep->closeCursor();
        return $this->val;
    }
   

    // -- charge via sql une ligne et la renvoie -- //
    // @indexId : valeur de l'index a charger
    // @selectedId : valeur de l'index a mettre en selected
    public function ligneInDb($indexId){
        $this->setSqlLast('SELECT *  FROM '.$this->tableNom
            .   ' WHERE '.$this->indexNom.' = '. $indexId
        );
        $rep=$this->db->query($this->sqlLast);
        $ligne=$rep->fetch(PDO::FETCH_ASSOC);
        $rep->closeCursor();
        return $ligne;
    }


    // - --------- - //
    // - affichage - //
    // - --------- - //

    // -- charge via sql puis affiche les valeurs d'un champ sous forme d'options -- //
    // @fieldName : nom du champ a afficher
    // @selectedId : valeur de l'index a mettre en selected
    // @descriptionFieldName : nom du champ contenant la description (utiliser pour le label)
    public function showSelectOptionsInDb($fieldName,$selectedId=NULL,$descriptionFieldName=NULL){
        $descriptionSql=$descriptionFieldName !== NULL ? ' ,`'.$descriptionFieldName.'` AS title': '';
        $this->setSqlLast('SELECT '.$this->indexNom.','.$fieldName.$descriptionSql.' FROM '.$this->tableNom);
        $o='';
        try{
            $rep=$this->db->query($this->sqlLast);

            while ($ligne=$rep->fetch(PDO::FETCH_ASSOC)){

                //if (ISDEV >= 1)echo  gestLib_inspect('$ligne',$ligne,__FILE__.':'.__LINE__,__FUNCTION__);

                $id=$ligne[$this->indexNom];
                $selected=($id == $selectedId)?' selected  class="selected"':'';
                $title=isset($ligne['title'])?' title="'.$ligne['title'].'"':'';
                $o.='<option value="'.$id.'"'.$title.$selected.'>'.$ligne[$fieldName].'</option>'."\n";
            };
            $rep->closeCursor();
        }
        catch(Exception $e){
            echo 'Erreur dans la structure sql:'.$this->sqlLast.'<br />';
        }
        return $o;
    }//function showSelectOptionsInDb()


    // -- affiche les valeurs d'un champ sous forme d'options -- //
    // @fieldName : nom du champ a afficher
    // @selectedId : valeur de l'index a mettre en selected
    public function showSelectOptions($fieldName,$selectedId=NULL){
        $o='';

        foreach ($this->table as $ligne){
            $id=$ligne[$this->indexNom];
            $selected=($id === $selectedId)?' selected  class="selected"':'';
            $o.='<option value="'.$id.'" title=""'.$selected.'>'.$ligne[$fieldName].'</option>'."\n";
        }
        return $o;
    }//function showSelectOptions()


    // -- affiche un formulaire de choix du numéro de page -- //
    public function showPageNoSelectOptions(){
        $o='';

        for ($pageNu=1;$pageNu<=$this->pageNb;$pageNu++){
            $selected=($pageNu === $this->pageNo)?' selected  class="selected"':'';
            $o.='<option value="'.$pageNu.'" title=""'.$selected.'>'.$pageNu.'</option>'."\n";
        }
        return $o;
    }//function showPageNoSelectOptions()


    // -- affiche un formulaire de choix du nombre d'élément par page -- //
    public function showEltByPageSelectOptions(){
        $o='';
        $selectedOk=false;//pas de selected concordant

        // - tous - //
        $o.='<option value="0" title="">Tous</option>'."\n";

        // - 1 - //
        if ($selected=(1 === $this->eltByPage)){
                $selected=' selected  class="selected"';$selectedOk=true;
            }
            else{$selected='';}
            $o.='<option value="1" title=""'.$selected.'>1</option>'."\n";

        for ($eltNu=5;$eltNu<=50;$eltNu+=5){
            if ($selected=($eltNu === $this->eltByPage)){
                $selected=' selected  class="selected"';$selectedOk=true;
            }
            else{$selected='';}
            $o.='<option value="'.$eltNu.'" title=""'.$selected.'>'.$eltNu.'</option>'."\n";
        }
        if ($selectedOk === false) 
            $o.='<option value="'.$this->eltByPage.'" title="" selected  class="selected"> '.$this->eltByPage.'</option>'."\n";

        return $o;
    }//function showEltByPageSelectOptions()


    // - affiche les champs d'une ligne - //
    // @id : 
    //  - non NULL: remplir les champs avec les valeurs du champs de l'index
    //  - NULL: pas de valeur par defaut
    public function showContent($id=NULL){
        $o="$id: ";
        // 
        foreach ($this->table($id) as $fieldId => $val){
            //echo gestLib_inspect('$fieldName',$fieldName);
            //echo gestLib_inspect('$val',$val);
            if (is_array($val)){    // si c'est une ligne 
                $o.=$this->showContent($fieldId);     // alors on renvoie son id
            }
            else{
                $o.=' <b>'.$fieldId.'</b> ='.$val;
            }
        }
        return $o.'<br />'."\n";
    }//function showContent


    // - affichage standart d'un champ input - //
    // @fielName : nom du champ de la table (for, name,id
    // @fieldTxt : texte pour l'utilisteur
    // @val : valeur du champ
    // @placeholder : placeholder
    public function showFormInput($fielName,$fieldTxt,$val='',$placeholder=''){
            $o.='<span class="labelChamp">';
            $o.='<label for="'.$fielName.'">'.$fielTxt.'</label>: ';
            $o.='<input name="'.$fielName.'" id="'.$fielName.'" placeholder="'.$placeholder.'" value="'.$val.'" />';
            $o.='</span>'."\n";
    }

} //class GestTable

$gestLib->libs['GestTable']->git='';
$gestLib->end('GestTable');

