<?php
//require('vendors/Notifs/Notifs.php');
require('vendors/GestTable/GestTable.php');
//require('vendors/GestTable/GestTableAdmin.php');

try{
    $db=dbInit();
}
catch (Exception $e){
    //die('Erreur : ' . $e->getMessage());
    echo $e->getMessage();
    
    // detailler les exeptions
    // par defaut quitter
    die('<br />Probleme avec la base de données. Le programme est interompu.');
}



$tableTest=new GestTable($db,TBLPREFIXE.'sandBox','jours_id','gestBase');

// - Chargement - //
$tableTest->setWhere('');
//$tableTest->setGroupby('mri_cantonId');
$tableTest->setOrderby('jours_id','DESC');

//$tableTest->load();
//$tableTest->load($pageNo=2);
$tableTest->load($pageNo=3,$eltByPage=10);
if(ISDEV === 1)echo '<span class="coding_filename">'.__FILE__.':'.__LINE__.'</span><br><div class="coding_code">'.nl2br($tableTest->sqlLast()).'</div>';


