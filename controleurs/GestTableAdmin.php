<?php
require('vendors/Notifs/Notifs.php');
require('vendors/GestTable/GestTable.php');
require('vendors/GestTable/GestTableAdmin.php');

try{
    $db=dbAdminInit();
}
catch (PDOException $e){
    //die('Erreur : ' . $e->getMessage());
    echo $e->getMessage();
    
    // detailler les exeptions
    // par defaut quitter
    die('Probleme avec la base de données. Le programme est interompu.');
}


$admin=new GestTableAdmin($db,TBLPREFIXE.'ephemeride','jours_id','eph');

$admin->checkUpdate();

// - Chargement - //
$admin->setWhere('');
//$admin->setGroupby('mri_cantonId');
$admin->setOrderby('jours_id','DESC');

$admin->load($pageNo=NULL,$eltByPage=NULL);



