<?php
error_reporting(E_ALL);
//error_reporting(NULL);

define('VERSIONSTATIQUE',0);
if (defined('VERSIONSTATIQUE') AND VERSIONSTATIQUE === 1){
	error_reporting(NULL);
	//display_errors(0);
}

// - docs communs- //
define('DOCUMENT_ROOT',$_SERVER['DOCUMENT_ROOT'].'/');
define('INTERSITES_ROOT','./');
define('_0PAGES_ROOT','/www/0pages/');
define('PAGES_ROOT','./pages/');
define('_0TEXTES_ROOT',DOCUMENT_ROOT.'0textes/');
define('_0ARTICLES_ROOT',DOCUMENT_ROOT.'0articles/');

// - docs specifiques au projet - //
define('MENU_ROOT','./menus/');

