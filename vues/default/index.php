<!DOCTYPE html><html lang="fr">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="Content-Style-Type" content="text/css">
    <meta http-equiv="Content-Script-Type" content="text/javascript">
    <meta name="robots" content="noindex,nofollow">
    <meta name="author" content="Pascal TOLEDO">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="generator" content="vim">
    <meta name="identifier-url" content="http://legral.fr">
    <meta name="date-creation-yyyymmdd" content="20151216">
    <meta name="date-update-yyyymmdd" content="20160107">
    <meta name="reply-to" content="pascal.toledo@legral.fr">
    <meta name="revisit-after" content="never">
    <meta name="category" content="">
    <meta name="publisher" content="legral.fr">
    <meta name="copyright" content="pascal TOLEDO">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>GestTable</title>
    <?php //echo $meta;unset($meta);?>
    <!-- scripts -->
    <script src="./locales/scripts<?php echo MINIMIZE_SUFFIXE?>.js"> </script>

    <!-- styles -->
    <link rel="stylesheet" href="./styles/styles<?php echo MINIMIZE_SUFFIXE?>.css" media="all" />
</head>

<body>
<div id="page">

    <!-- header -->
    <header id="header">
        <div id="headerGauche"><!--gauche--></div>
            <h1><a href="https://legral.fr/">GestTable</a> : <a href="?">accueil</a></h1>

            <div class="ms">
                <ul>
<?php $css=PAGE==='accueil'?' class="actif"':'';?>
                    <li<?php echo $css?>><a<?php echo $css?> href="?GestTable=accueil" title="">Accueil</a></li>
<?php $css=PAGE==='GestTable'?' class="actif"':'';?>
                    <li<?php echo $css?>><a<?php echo $css?> href="?GestTable=GestTable" title="">GestTable</a></li>
<?php $css=PAGE==='GestTableAdmin'?' class="actif"':'';?>
                    <li<?php echo $css?>><a<?php echo $css?> href="?GestTable=GestTableAdmin" title="">GestTableAdmin</a></li>
                </ul>
            </div>
            <div><a href="?ISDEV=1&amp;DEBUG_LVL=1">Activer le mode debug</a></div>


            <div id="headerDroit">
                <a href="?deconnect=1">D&eacute;connecter</a>
                <a href="?about=accueil">&agrave; propos de...</a>
            </div>
    </header><!-- header -->

<section id="pageContent">
<?php
include 'vues/default/'.PAGE.'.php';
?>
</section>
</div><!-- //page -->

<?php
    include 'vues/default/footer.php';
?>

<section id="dev">
<?php
if (ISDEV > 0){
    echo gestLib_inspect('PAGE',PAGE);
    echo gestLib_inspect('$_GET',$_GET);
    echo gestLib_inspect('$_POST',$_POST);
    echo gestLib_inspect('$_SESSION',$_SESSION);
}

?>
</section>
<script>
openTag();
</script>
<a id="end"></a>
</body></html>
