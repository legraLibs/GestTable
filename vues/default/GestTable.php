<h2>table</h2>
<?php
echo gestLib_inspect('$tableTest',$tableTest);
echo gestLib_inspect('$tableTest->table(-1)',$tableTest->table(-1));
echo gestLib_inspect('$tableTest->table($tableTest->indexMax())',$tableTest->table($tableTest->indexMax()));
?>
<h2>variables</h2>

<?php
echo gestLib_inspect('$tableTest->pageNo()',$tableTest->pageNo());
echo gestLib_inspect('$tableTest->eltByPage()',$tableTest->eltByPage());
echo gestLib_inspect('$tableTest->pageNb()',$tableTest->pageNb());
echo gestLib_inspect('$tableTest->eltFirst()',$tableTest->eltFirst());
?>
<h2>index</h2>
<?php
echo gestLib_inspect('$tableTest->lastInsertId()',$tableTest->lastInsertId());
echo gestLib_inspect('$tableTest->indexMax()',$tableTest->indexMax());
echo gestLib_inspect('$tableTest->indexMaxInDb()',$tableTest->indexMaxInDb());
?>
<h2>gestion du nombre d'élément dans une page ou dans la table ou dans la db</h2>

<?php
echo gestLib_inspect('$tableTest->eltInDb(GestTable::CACHE)',$tableTest->eltInDb(GestTable::CACHE));
echo gestLib_inspect('$tableTest->eltInDb(GestTable::NOCACHE)',$tableTest->eltInDb(GestTable::NOCACHE));
echo gestLib_inspect('$tableTest->eltInDb()',$tableTest->eltInDb());
?>
<br />

<a name="Nav"></a>
<nav>
<h2>Navigation (<?php echo $tableTest->pageNo()?>/<?php echo $tableTest->pageNb()?>)<br />

<h3>pageNo</h3>

 GET
 <a href="?<?php echo ARIANE.'&amp;'.$tableTest->tag().'_pageNo=1'?>#Nav">_pageNo=1</a>
  <a href="?<?php echo ARIANE.'&amp;'.$tableTest->tag().'_pageNo=1000'?>#Nav">_pageNo=1000</a>
  <a href="?<?php echo ARIANE.'&amp;'.$tableTest->tag().'_pageNo=txt'?>#Nav">_pageNo=txt</a>
<br />

 POST
 <form action"?<?php echo ARIANE?>" method="GET" id="fPageNo" >
       no de page:
    <select onchange="window.location.href='?<?php echo ARIANE.'&amp;'.$tableTest->tag().'_pageNo';?>=' + this[this.selectedIndex].value+'#Nav'">
    <!--select id="id" name="id" onchange="document.forms["id_form"].submit();"-->
        <?php echo $tableTest->showPageNoSelectOptions();?>
    </select>

    nombre d'élément par page:<select onchange="window.location.href='?<?php echo ARIANE.'&amp;'.$tableTest->tag().'_eltByPage';?>=' + this[this.selectedIndex].value+'#Nav'">
    <!--select id="id" name="id" onchange="document.forms["id_form"].submit();"-->
        <?php echo $tableTest->showEltByPageSelectOptions();?>
    </select>
</form>
<br />

   <a href="?<?php echo ARIANE.'&amp;'.$tableTest->tag().'_relPage'?>#Nav">pr&eacute;c&eacute;dent</a>
 - <a href="?<?php echo ARIANE.'&amp;'.$tableTest->tag().'_relPage=-1'?>#Nav">pr&eacute;c&eacute;dent(-1)</a>
 - <a href="?<?php echo ARIANE.'&amp;'.$tableTest->tag().'_relPage=-5'?>#Nav">pr&eacute;c&eacute;dent(-5)</a>

<br />
   <a href="?<?php echo ARIANE.'&amp;'.$tableTest->tag().'_relPage'?>#Nav">suivant</a>
 - <a href="?<?php echo ARIANE.'&amp;'.$tableTest->tag().'_relPage=1'?>#Nav">suivant(1)</a>
 - <a href="?<?php echo ARIANE.'&amp;'.$tableTest->tag().'_relPage=5'?>#Nav">suivant(5)</a>

</nav>

<h2>Affichage</h2>

<h3>Afficher un champs table(id=$tableTest->indexMax(),$fieldName='fete')</h3>
<?php echo $tableTest->table($id=$tableTest->indexMax(),$fieldName='fete');?>


<h3>showSelectOptions()</h3>
<select>
<?php echo $tableTest->showSelectOptions($fieldName='fete',$selectedId=7);?>
</select><br />

<h3>showSelectOptionsInDb('fete')</h3>
<select>
    <?php echo $tableTest->showSelectOptionsInDb($fieldName='fete',$selectedId=7);?>
</select><br />


<h3>ligneInDb()</h3>
<?php
    echo gestLib_inspect('$tableTest->ligneInDb(7)',$tableTest->ligneInDb(7));
    ?>
<br />



<h2>table</h2>
<?php
    echo gestLib_inspectOrigine('$tableTest->table()',$tableTest->table());
?>
<h2>table($tableTest->indexMax())</h2>
<?php
    echo gestLib_inspectOrigine('$tableTest->table($tableTest->indexMax())',$tableTest->table($tableTest->indexMax()));
?>


<a name="showContent"></a>
<h2>showContent($tableTest->indexMax())</h2>
<?php
    echo $tableTest->showContent($tableTest->indexMax());
?>

<h2>showContent()</h2>
<?php
    echo $tableTest->showContent();
?>

<h2>sql</h2>
<?php
echo gestLib_inspect('$tableTest->sqlLast()',$tableTest->sqlLast());
echo gestLib_inspect('$tableTest->sqls()',$tableTest->sqls());
?>



<?php

//echo $getLib->table();