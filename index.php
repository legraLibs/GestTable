<?php
require('config/config.php');
require('configLocal/config.php');
require('configLocal/configDB.php');

//  === bufferisation ====  //
ob_start();


//  ==== session ==== //
session_name('gestTable_session');
session_start();
session_regenerate_id();

//  === gestionnaire de librairies ====  //
require('vendors/gestLib/gestLib.php');
$gestOut->setIsReturn(1);
$gestOut->setIsFile(0);
//echo $gestOut;

$pspV=0;
if (isset($_SESSION['ISDEV']))$pspV=(int)$_SESSION['ISDEV'];
if (isset($_POST['ISDEV']))$pspV=(int)$_POST['ISDEV'];
if (isset($_GET['ISDEV']))$pspV=(int)$_GET['ISDEV'];
define('ISDEV',$pspV);
$_SESSION['ISDEV']=ISDEV;

$pspV=0;
if (isset($_SESSION['DEBUG_LVL']))$pspV=(int)$_SESSION['DEBUG_LVL'];
if (isset($_POST['DEBUG_LVL']))$pspV=(int)$_POST['DEBUG_LVL'];
if (isset($_GET['DEBUG_LVL']))$pspV=(int)$_GET['DEBUG_LVL'];
define('DEBUG_LVL',$pspV);
$_SESSION['DEBUG_LVL']=DEBUG_LVL;


//define('MINIMIZE_SUFFIXE',ISDEV===1?'':'.min');
define('MINIMIZE_SUFFIXE',''); // pas de mode minimiser


if ( isset($_GET['deconnect']) && $_GET['deconnect'] == 1 ){
    $_SESSION = array();
    session_destroy();
}

//  === Verification password general ====  //
/*
$p='';
if (isset($_POST['p']))
    {$p=$_POST['p'];}
elseif (isset($_SESSION['p']))
    {$p=$_SESSION['p'];}

if ($p !== PWD ){
    require PAGESLOCALES_ROOT.'parrainages/vues/passAsk.php';
    exit(0);
}
$_SESSION['p'] = $p;
unset($p);
 */

//  ==== constantes generales ==== //
define('DATENOW',date('Y/m/d'));




// = ============================== = //
// = chargement de la configuration = //
// = ============================== = //


// = ========== = //
// = controleur = //
// = ========== = //
define('PAGE',isset($_GET['GestTable'])?$_GET['GestTable']:'accueil');

define ('ARIANE', 'GestTable='.PAGE);
$controleurFile='accueil.php';
switch (PAGE){
    case'GestTable':$controleurFile=PAGE.'.php'; break;
    case'GestTableAdmin':$controleurFile=PAGE.'.php'; break;
}


require('controleurs/'.$controleurFile);



//  ==== html ==== //
include ('vues/default/index.php');


